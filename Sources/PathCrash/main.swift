import Foundation
import System


if #available(macOS 12.0, *) {
	let relative = FilePath("Versions")
	_ = FilePath("/a/b/c").appending(relative.components)
	print(relative)
}
